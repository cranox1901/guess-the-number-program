// GuessTheNumberProgram.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Include the library so that we can read and write to the console
#include <iostream>

//Include the library so we can use the random function
#include <stdlib.h>

//Include the library so we can access the current time
#include <time.h>

int main()
{
    //-----------------------------------------------------------------
    // Print the title of the game
    //-----------------------------------------------------------------

    //Print text to the console using cout
    std::cout << "Guess The Number" << std::endl;
    std::cout << "By Ethan Paterson" << std::endl;
    std::cout << std::endl;
    std::cout << "Guess the random number in as few guesses." << std::endl;
    std::cout << std::endl;

    //Make the program pause before executing the next statement
    system("PAUSE");

    //Declare and initialise the variable restartResponse to 'y'
    char restartResponse = 'y';

    while (restartResponse == 'y')
    {

        //-----------------------------------------------------------------
        // Generate a random number
        //-----------------------------------------------------------------

        //Initialise the seed random function to the current time
        srand(time(NULL));

        //Declare and initialise the variable secretNumber to 0
        int secretNumber = 0;

        //Generate a random number between 1 and 10
        secretNumber = rand() % 10 + 1;

        //-----------------------------------------------------------------
        // Guessing section
        //-----------------------------------------------------------------

        //Declare and initialise the variable guess to 0
        int guess = 0;

        //Declare and initialise the variable numAttempts to 0
        int numAttempts = 0;

        //Allow the user to guess until they are correct
        while (guess != secretNumber)
        {
            //Ask the player to guess a number between 1 and 10
            std::cout << "Please guess a number between 1 and 10:" << std::endl;
            std::cout << "> ";

            //Read the player input and add it to the guess variable
            std::cin >> guess;

            //Increment the number of attepts after each guess
            numAttempts = numAttempts + 1;

            //Display the player's guess
            std::cout << std::endl;
            std::cout << "You guessed: " << guess << std::endl;
            std::cout << std::endl;

            //Check the player's guess
            if (guess == secretNumber)
            {
                //If the guess is correct
                std::cout << "Your guess was correct." << std::endl;
                std::cout << std::endl;

                //Display the number of attemps
                std::cout << "It took you " << numAttempts << " attempts to guess correctly." << std::endl;
                std::cout << std::endl;

                //Ask if the player would like to play again
                std::cout << "If you would like to play again, type y." << std::endl;
                std::cout << std::endl;

                //Read the user response and add it to the variable
                std::cin >> restartResponse;
            }
            else
            {
                //If the guess is incorrect
                std::cout << "Your guess was incorrect." << std::endl;
                if (guess < secretNumber)
                {
                    //If the guess was too low
                    std::cout << "Your guess was too low." << std::endl;
                    std::cout << "> ";
                }
                else
                {
                    //If the guess was too high
                    std::cout << "Your guess was too high." << std::endl;
                    std::cout << std::endl;
                }
            }
        }
    }

    //-----------------------------------------------------------------
    // End the program
    //-----------------------------------------------------------------

    //Close the console
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
